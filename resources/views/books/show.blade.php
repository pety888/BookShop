@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card text-center">
                    <div class="card-header">
                        <h1>Author's books</h1>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>NAME</th>
                                <th>YEAR OF ISSUE</th>
                                <th>COUNT OF CHAPTERS</th>
                                <th>TAGS</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($books as $key => $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ number_format($item->year_of_issue,0,',',' ') }}</td>
                                    <td>{{ number_format($item->count_of_chapters) }}</td>
                                    <td>
                                        @foreach($item->tags as $value)
                                            <h4><span class="badge badge-success">
                                                {{ $value->tag }}
                                            </span></h4>
                                        @endforeach
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-dark" href="{{ route('authors.index') }}">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection