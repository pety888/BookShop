@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card text-center">
                    <div class="card-header">
                        <h1>Tags</h1>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>TAG</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tags as $key => $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->tag }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        {{ $tags->links() }}
                        <a class="btn btn-dark" href="{{ route('authors.index') }}">Go to Authors list</a>
                        <a class="btn btn-dark" href="{{ route('books.index') }}">Go to Books list</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection