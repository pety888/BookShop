@extends('layouts.app')

@section('content')
    <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card text-center">
                <div class="card-header">
                    <h1>Authors</h1>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>VORNAME</th>
                                <th>LASTNAME</th>
                                <th>AGE</th>
                                <th>List of books</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($authors as $key => $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->vorname }}</td>
                                    <td>{{ $item->lastname }}</td>
                                    <td>{{ number_format($item->age) }}</td>
                                    <td>
                                        <a class="btn btn-primary" href="{{ route('books.show', ['id' => $item->id]) }}">Show</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    {{ $authors->links() }}
                    <a class="btn btn-info" href="{{ route('books.index') }}">List of all books</a>
                    <a class="btn btn-warning" href="{{ route('tags.index') }}">List of all tags</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection