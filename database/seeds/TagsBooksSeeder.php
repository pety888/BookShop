<?php

use Illuminate\Database\Seeder;

class TagsBooksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\TagsBooks::class, 15)->create();
    }
}
