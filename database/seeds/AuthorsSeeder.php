<?php

use Illuminate\Database\Seeder;

class AuthorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $authors = factory(App\Author::class, 5)->create();
        $authors->each(function($author) use ($authors) {
            factory(App\Book::class, 2)
                ->create(['author_id' => $author->id]);
        });
    }
}
