<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tag', function (Blueprint $table) {
           $table->increments('id');
           $table->string('tag');
           $table->timestamps();
        });

        \App\Tag::insert([
           ['tag' => 'thriller', 'created_at' => now(), 'updated_at' => now()],
           ['tag' => 'fantasy', 'created_at' => now(), 'updated_at' => now()],
           ['tag' => 'drama', 'created_at' => now(), 'updated_at' => now()],
           ['tag' => 'action', 'created_at' => now(), 'updated_at' => now()],
           ['tag' => 'adventurous', 'created_at' => now(), 'updated_at' => now()]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tag');
    }
}
