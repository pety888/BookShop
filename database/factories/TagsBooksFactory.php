<?php

use Faker\Generator as Faker;

$factory->define(App\TagsBooks::class, function (Faker $faker) {
    return [
        'book_id' => $faker->numberBetween(1,App\Book::count()),
        'tag_id' => $faker->numberBetween(1,App\Tag::count())
    ];
});
