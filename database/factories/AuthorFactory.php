<?php

use Faker\Generator as Faker;

$factory->define(App\Author::class, function (Faker $faker) {
    return [
        'vorname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'age' => $faker->numberBetween(30,50),
    ];
});
