<?php

use Faker\Generator as Faker;

$factory->define(App\Book::class, function (Faker $faker) {
    return [
        'name' => $faker->words(5,true),
        'year_of_issue' => $faker->date('Y'),
        'count_of_chapters' => $faker->numberBetween(15,30),
        'author_id' => $faker->numberBetween(1,App\Author::count())
    ];
});
