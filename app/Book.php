<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'book';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function authors()
    {
        return $this->belongsTo('App\Author','author_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany('App\Tag','tags_books','book_id','tag_id');
    }
}
