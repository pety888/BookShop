<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagsBooks extends Model
{
    protected $table = 'tags_books';
}
